export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAvcpwleb7vDBpVYtugDCqFdq--a60MTvE",
    authDomain: "gplx-48ef2.firebaseapp.com",
    databaseURL: "https://gplx-48ef2.firebaseio.com",
    projectId: "gplx-48ef2",
    storageBucket: "gplx-48ef2.appspot.com",
    messagingSenderId: "403946066395",
    appId: "1:403946066395:web:55b984127f27178be857fe",
    measurementId: "G-X2EEMYH14P"
  },
  defaultAnswerCount: 4, //Answer input count,
  snackBarConfig: {
    duration: 2 * 1000
  },
  ONESIGNAL_APP_ID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  ONESIGNAL_REST_API_KEY: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
};
