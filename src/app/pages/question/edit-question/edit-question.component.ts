import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
  FormArray
} from "@angular/forms";
import { AngularFireStorage } from "@angular/fire/storage";
import { Observable } from "rxjs";

import { QuestionService } from "src/app/services/question.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Question } from "src/app/models/question.model";
import { Answer } from "src/app/models/answer.model";
import {finalize} from "rxjs/operators";

@Component({
  selector: "app-edit-question",
  templateUrl: "./edit-question.component.html",
  styleUrls: ["./edit-question.component.css"]
})
export class EditQuestionComponent implements OnInit {
  questionForm: FormGroup;
  quizKey: string;
  key: string;
  correctAnswer: string;
  image_url;

  private downloadURL: Observable<string>;

  constructor(
    private formBuilder: FormBuilder,
    private questionService: QuestionService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private storage: AngularFireStorage
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => {
      if (data["quizKey"] && data["key"]) {
        this.quizKey = data["quizKey"];
        this.key = data["key"];
        this.getQuestionDetail(data["quizKey"], data["key"]);
      }
    });
  }

  getQuestionDetail(quizKey, key) {
    this.questionService.get(quizKey, key).subscribe(data => {
      data.forEach(element => {
        if (element.key == key) {
          this.createForm(element as Question);
          this.correctAnswer = element.answerRight;
          this.image_url = element.image;
        }
      });
    });
  }

  createForm(question: Question) {
    this.questionForm = this.formBuilder.group({
      question: new FormControl(question.question, [Validators.required]),
      image: new FormControl(question.image),
      answerRight: new FormControl(question.answerRight, [Validators.required]),
      status: new FormControl(question.status),
      answers: this.formBuilder.array(question.answers)
    });
  }

  onQuestionFormSubmit(values): void {
    if (this.questionForm.valid) {
      var question = values as Question;
      this.questionService.update(question, this.quizKey, this.key);
      this.router.navigateByUrl(`question/${this.quizKey}`);
      //this.resetForm();
    }
  }

  onFileSelected(event) {
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `questions/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`questions/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.image_url = url;
              this.questionForm.controls.image.setValue(this.image_url)
              // this.questionForm.controls.image.setValue(`<img src='${this.image_url}' />`)
            }
            console.log(this.image_url);
          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);
        }
      });
  }

  resetForm() {
    this.questionForm.reset();
  }

  getAnswerInit(answers: Answer[]) {
    let arr = [];
    for (let i = 0; i < answers.length; i++) {
      arr.push(answers[i]);
    }
    return arr;
  }
}
